#! /usr/bin/make -f
# -*- makefile -*-
# ex: set tabstop=4 noexpandtab:
# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

default: help all
	-@date -u

project?=pim435
url?=https://booting.oniroproject.org/rzr/pim435

# Configuration flags
#CFLAGS+=-DDEBUG=1
CFLAGS+=-Isrc
LDLIBS+=-li2c

# Default flags
exe?=src/examples/${project}-example
lib_srcs+=src/${project}/${project}.c
lib_headers+=src/${project}/${project}.h
lib_objs?=${lib_srcs:.c=.o}
lib?=lib${project}.a
exe_srcs+=${exe}.c
exe_objs+=${srcs:.c=.o}
srcs?=${lib_srcs} ${exe_srcs}

prefix?=/usr/local
includedir?=${prefix}/include
base_bindir?=/bin
base_libdir?=/lib
bindir?=${prefix}/${base_bindir}
libdir?=${prefix}/${base_libdir}

sysroot?=${CURDIR}/tmp/sysroot
CFLAGS+=-I${sysroot}${includedir}/
LDLIBS+=-L${sysroot}/${libdir}


help:
	@echo "# URL: ${url}"
	@echo "# Usage:"
	@echo "#  make setup # will install build tools"
	@echo "#  make run # will run demo"

lib: ${lib}
	stat $^

${lib}: ${lib_objs}
	${AR} crU $@ $^

${exe}: ${exe_objs} ${lib}
	${CC} ${LDFLAGS} -o $@ $^ ${LDLIBS}

all: ${exe}
	stat $<

run: ${exe}
	${sudo} ${<D}/${<F} ${args}

clean:
	rm -f *.o */*.o */*/*.o *.a src/examples/pim435-example

cleanall: clean
	rm -f ${exe} ${lib}

install: ${exe} ${lib} ${lib_headers}
	${sudo} install -d "${DESTDIR}/${bindir}"
	${sudo} install $< "${DESTDIR}/${bindir}/${project}"
	${sudo} install -d "${DESTDIR}/${includedir}/${project}"
	${sudo} install ${lib_headers} "${DESTDIR}/${includedir}/${project}"
	${sudo} install -d "${DESTDIR}/${libdir}"
	${sudo} install ${lib} "${DESTDIR}/${libdir}/"

setup/debian: /etc/debian_version
	-${sudo} apt-get update -y
	${sudo} apt-get install -y \
		gcc \
		libi2c-dev

/etc/debian_version:
	@echo "error: distro not supported please file a bug: ${url}"

setup: setup/debian
	@-date -u

format: ${srcs} ${lib_headers}
	for file in $^ ; do \
	  clang-format < $${file} > $${file}.tmp ; \
	  mv -v $${file}.tmp $${file} ; \
	done

demo/cycle: ${exe}
	for r in 0 255 0 ; do for g in 0 255 0 ; do for b in 0 255 0 ; do \
	${exe} \
$${r} $${g} $${b}  $${r} $${g} $${b}  $${r} $${g} $${b}  $${r} $${g} $${b}  $${r} $${g} $${b} \
$${r} $${g} $${b}  $${r} $${g} $${b}  $${r} $${g} $${b}  $${r} $${g} $${b}  $${r} $${g} $${b} \
$${r} $${g} $${b}  $${r} $${g} $${b}  $${r} $${g} $${b}  $${r} $${g} $${b}  $${r} $${g} $${b} \
$${r} $${g} $${b}  $${r} $${g} $${b}  $${r} $${g} $${b}  $${r} $${g} $${b}  $${r} $${g} $${b} \
$${r} $${g} $${b}  $${r} $${g} $${b}  $${r} $${g} $${b}  $${r} $${g} $${b}  $${r} $${g} $${b} \
; sleep 1; done ; done ; done

demo/%:
	${exe} \
${@F} ${@F} ${@F}  ${@F} ${@F} ${@F}  ${@F} ${@F} ${@F}  ${@F} ${@F} ${@F}  ${@F} ${@F} ${@F}  \
${@F} ${@F} ${@F}  ${@F} ${@F} ${@F}  ${@F} ${@F} ${@F}  ${@F} ${@F} ${@F}  ${@F} ${@F} ${@F}  \
${@F} ${@F} ${@F}  ${@F} ${@F} ${@F}  ${@F} ${@F} ${@F}  ${@F} ${@F} ${@F}  ${@F} ${@F} ${@F}  \
${@F} ${@F} ${@F}  ${@F} ${@F} ${@F}  ${@F} ${@F} ${@F}  ${@F} ${@F} ${@F}  ${@F} ${@F} ${@F}  \
${@F} ${@F} ${@F}  ${@F} ${@F} ${@F}  ${@F} ${@F} ${@F}  ${@F} ${@F} ${@F}  ${@F} ${@F} ${@F}  \
; sleep 1

demo/fade:
	for c in $$(seq 0 255) ; do ${MAKE} demo/$${c} ; done


demo: demo/0 demo/255 demo/cycle demo/fade demo/00
