<!--
SPDX-FileCopyrightText: Huawei Inc.

SPDX-License-Identifier: MIT
-->

# PIM435

[![license](
https://img.shields.io/badge/license-MIT-0.svg
)](MIT)

[![IRC Channel](
https://img.shields.io/badge/chat-on%20libera-brightgreen.svg
)](
irc://libera.chat/#oniroproject
)

## Introduction

[PIM435](https://shop.pimoroni.com/products/5x5-rgb-matrix-breakout) is a 5x5
RGB LED matrix by Pimoroni which provides a sample application written in
Python. This project implements a similar userspace driver application, written
in C as a static library. Both projects rely on
[i2c-dev](https://www.kernel.org/doc/Documentation/i2c/dev-interface) for
communication with the i2c device.

## Demo

[![PIM435-2021-rzr](
https://diode.zone/lazy-static/previews/38602ec4-bb78-4476-a92f-b8c774356c4a.jpg
)](
https://diode.zone/w/nQWJKynRP84FgNb3DDH32m#PIM435-2021-rzr
"PIM435-2021-rzr")

# Resources

- <https://www.adafruit.com/product/4830>
- <https://github.com/pimoroni/rgbmatrix5x5-python/>
