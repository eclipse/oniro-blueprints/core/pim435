// SPDX-FileCopyrightText: Huawei Inc.
//
// SPDX-License-Identifier: MIT

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <pim435/pim435.h>

int main(int argc, char *argv[]) {
  int bus = getenv("PIM435_I2C_BUS") ? atoi(getenv("PIM435_I2C_BUS"))
                                     : PIM435_I2C_BUS;
  pim435_context *context = pim435_open(bus, PIM435_I2C_ADDRESS);

  if (context == NULL) {
    printf("error: io: pim435\n");
    exit(1);
  }
  for (int i = 0; i < (argc - 1) / 3; i++) {
    unsigned char red = atoi(*++argv);
    unsigned char green = atoi(*++argv);
    unsigned char blue = atoi(*++argv);

    int x = i / PIM435_VIEWPORT_WIDTH;
    int y = i % PIM435_VIEWPORT_WIDTH;

    pim435_set_pixel(context, x, y, red, green, blue);
  }
  pim435_show(context);
  pim435_close(context);
  free(context);
}
