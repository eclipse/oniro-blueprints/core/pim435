// SPDX-FileCopyrightText: Huawei Inc.
//
// SPDX-License-Identifier: MIT
// Inspired from https://github.com/pimoroni/rgbmatrix5x5-python/ (MIT)

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include <i2c/smbus.h>
#include <linux/i2c-dev.h>

#include "pim435.h"

// IS31FL3731  uses a 144 char buffer, for 2 non squared matrix of LED
#define PIM435_FRAMEBUFFER_LENGHT (8 * 9 * 2)

typedef struct pim435_context {
  int fd;
  int is_setup;
  int current_frame;
  unsigned char framebuffer[PIM435_FRAMEBUFFER_LENGHT];
} pim435_context;

void pim435_print(pim435_context *context);

int pim435_setup(pim435_context *context);

void pim435_reset(pim435_context *context);

void pim435_frame(pim435_context *context, int frame);

int pim435_bank(pim435_context *context, unsigned char bank);

int pim435_register(pim435_context *context, int bank, int reg, int value);

#ifndef MAX_PATH_LEN
#define MAX_PATH_LEN 260
#endif

#ifdef DEBUG
#define debugf printf
#else
#define debugf(...)
#endif

/// https://www.lumissil.com/assets/pdf/core/IS31FL3731_DS.pdf
enum registers {
  MODE_REGISTER = 0x00,
  FRAME_REGISTER = 0x01,
  AUDIOSYNC_REGISTER = 0x06,
  SHUTDOWN_REGISTER = 0X0A,
};

enum constants {
  CONFIG_BANK = 0X0B,
  BANK_ADDRESS = 0XFD,
  PICTURE_MODE = 0x00,
  AUDIOPLAY_MODE = 0x18,
  ENABLE_OFFSET = 0x00,
  COLOR_OFFSET = 0x24,
};

void pim435_print(pim435_context *context) {

  for (int chunk = 0; chunk <= PIM435_FRAMEBUFFER_LENGHT / 32; chunk++) {
    int offset = chunk * 32;
    int remain = (PIM435_FRAMEBUFFER_LENGHT - chunk * 32);
    int len = (remain >= 32) ? 32 : remain;
    for (int i = 0; i < len; i++) {
      debugf("%02x ",
             (unsigned char)*(context->framebuffer + offset + i) & 0xFF);
    }
    debugf("\n");
  }
  debugf("\n");
}

pim435_context *pim435_open(int bus, int address) {
  pim435_context *context = NULL;
  int fd = 0;
  char filename[MAX_PATH_LEN];

  context = (pim435_context *)calloc(1, sizeof(pim435_context));
  if (context == NULL) {
    return NULL;
  }

  snprintf(filename, MAX_PATH_LEN, "/dev/i2c-%d", bus);
  debugf("open: %s\n", filename);
  if ((fd = open(filename, O_RDWR)) <= 0) {
    return NULL;
  }
  if (ioctl(fd, I2C_SLAVE, address) != 0) {
    return NULL;
  }
  context->fd = fd;
  if (pim435_setup(context) != 0) {
    debugf("warning: setup failed\n");
  }

  return context;
}

int pim435_setup(pim435_context *context) {
  debugf("#{ setup: %d\n", context->is_setup);
  if (context->is_setup++) {
    return 0;
  }
  int error = 0;
  // define which of the 144 led addresses are valid for 5x5 RGB
  static const unsigned char enable_pattern[] = {
      0b00000000, 0b10111111, 0b00111110, 0b00111110, 0b00111111, 0b10111110,
      0b00000111, 0b10000110, 0b00110000, 0b00110000, 0b00111111, 0b10111110,
      0b00111111, 0b10111110, 0b01111111, 0b11111110, 0b01111111, 0b00000000,
  };

  pim435_reset(context);

  pim435_show(context);

  pim435_bank(context, CONFIG_BANK);

  debugf("I2C: picture 0x%02x, 0x%02x\n", MODE_REGISTER, PICTURE_MODE);
  if (i2c_smbus_write_byte_data(context->fd, MODE_REGISTER, PICTURE_MODE)) {
    goto failed;
  }

  debugf("I2C: audio: 0x%02x, 0x%02x\n", AUDIOSYNC_REGISTER, 0);
  if (i2c_smbus_write_byte_data(context->fd, AUDIOSYNC_REGISTER, 0)) {
    goto failed;
  }

  // Enable/disable leds
  for (int b = 1; b >= 0; b--) {
    pim435_bank(context, b);
    debugf("I2C: enable: 0x%02x, [%d]\n", 0x00, 18);
    if (i2c_smbus_write_i2c_block_data(context->fd, 0x00, 18, enable_pattern)) {
      goto failed;
    }
  }

  goto success;

failed:
  debugf("pim435: error: %x: \n", error);
  return -1;

success:
  debugf("#} setup\n");
  return 0;
}

int pim435_close(pim435_context *context) {
  debugf("#{ close\n");
  context->fd = close(context->fd);
  debugf("#} close\n");
  return context->fd;
}

void pim435_clear(pim435_context *context) {
  debugf("#{ clear\n");
  context->current_frame = 0;
  for (unsigned char i = 0; i < PIM435_FRAMEBUFFER_LENGHT; i++) {
    context->framebuffer[i] = 0;
  }
  debugf("#} clear\n");
}

void pim435_set_pixel(pim435_context *context, unsigned char x, unsigned char y,
                      unsigned char r, unsigned char g, unsigned char b) {

  debugf("#{ set_pixel [%d,%d]\n", x, y);
  static int lookup[][3] = {
      {118, 69, 85}, {117, 68, 101}, {116, 84, 100}, {115, 83, 99},
      {114, 82, 98}, {113, 81, 97},  {112, 80, 96},  {134, 21, 37},
      {133, 20, 36}, {132, 19, 35},  {131, 18, 34},  {130, 17, 50},
      {129, 33, 49}, {128, 32, 48},

      {127, 47, 63}, {121, 41, 57},  {122, 25, 58},  {123, 26, 42},
      {124, 27, 43}, {125, 28, 44},  {126, 29, 45},  {15, 95, 111},
      {8, 89, 105},  {9, 90, 106},   {10, 91, 107},  {11, 92, 108},
      {12, 76, 109}, {13, 77, 93},
  };

  if ((x >= PIM435_VIEWPORT_WIDTH) || (y >= PIM435_VIEWPORT_HEIGHT)) {
    debugf("warning: out of range values : %d, %d\n", x, y);
  }
  if ((x % 2) == 1) {
    y = 4 - y;
  }
  int p = y + x * 5;
  int rpos = lookup[p][0];
  int gpos = lookup[p][1];
  int bpos = lookup[p][2];

  context->framebuffer[rpos] = r;
  context->framebuffer[gpos] = g;
  context->framebuffer[bpos] = b;

  debugf("#} set_pixel\n");
}

int pim435_show(pim435_context *context) {
  debugf("#{ show %d\n", context->current_frame);
  if (pim435_setup(context) != 0) {
    debugf("warning: setup failed\n");
  }
  int next_frame = (context->current_frame == 1) ? 0 : 1;
  pim435_bank(context, next_frame);

  pim435_print(context);
  for (int chunk = 0; chunk <= PIM435_FRAMEBUFFER_LENGHT / 32; chunk++) {
    int offset = chunk * 32;
    int remain = (PIM435_FRAMEBUFFER_LENGHT - chunk * 32);
    int len = (remain >= 32) ? 32 : remain;
    if (0 != i2c_smbus_write_i2c_block_data(context->fd, COLOR_OFFSET + offset,
                                            len,
                                            context->framebuffer + offset)) {
      return (1 + chunk);
    }
  }
  pim435_frame(context, next_frame);
  debugf("#} show %d\n", next_frame);
  return 0;
}

void pim435_reset(pim435_context *context) {
  debugf("#{ reset\n");
  pim435_register(context, CONFIG_BANK, SHUTDOWN_REGISTER, 0);
  sleep(1);
  pim435_register(context, CONFIG_BANK, SHUTDOWN_REGISTER, 1);
  debugf("#} reset\n");
}

void pim435_frame(pim435_context *context, int frame) {
  debugf("# frame: f=%d\n", frame);
  context->current_frame = frame;
  pim435_register(context, CONFIG_BANK, FRAME_REGISTER, frame);
}

int pim435_bank(pim435_context *context, unsigned char bank) {
  debugf("I2C: bank: 0x%02x=0x%02x\n", BANK_ADDRESS, bank);
  int result = i2c_smbus_write_byte_data(context->fd, BANK_ADDRESS, bank);
  return result;
}

int pim435_register(pim435_context *context, int bank, int reg, int value) {
  debugf("#{ register: b=%02x, r=%02x v=%02x\n", bank, reg, value);
  pim435_bank(context, bank);
  debugf("I2C: register: 0x%02x=0x%02x\n", reg, value);
  int result = i2c_smbus_write_byte_data(context->fd, reg, value);
  debugf("#} register: b=%02x, r=%02x v=%02x\n", bank, reg, value);
  return result;
}
