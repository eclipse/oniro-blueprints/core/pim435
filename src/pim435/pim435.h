// SPDX-FileCopyrightText: Huawei Inc.
//
// SPDX-License-Identifier: MIT

#ifndef PIM453_H_
#define PIM453_H_

#include <stdbool.h>

// PIM435 only have 5x5 RGB leds wired
#define PIM435_VIEWPORT_WIDTH 5
#define PIM435_VIEWPORT_HEIGHT PIM435_VIEWPORT_WIDTH

// Default value, value works on RPi
#ifndef PIM435_I2C_BUS
#define PIM435_I2C_BUS 0x01
#endif

// Default value, Some devices have different address (0x77)
#ifndef PIM435_I2C_ADDRESS
#define PIM435_I2C_ADDRESS 0x74
#endif

typedef struct pim435_context pim435_context;

pim435_context *pim435_open(int bus, int address);

int pim435_close(pim435_context *context);

void pim435_clear(pim435_context *context);

void pim435_set_pixel(pim435_context *context, unsigned char x, unsigned char y,
                      unsigned char r, unsigned char g, unsigned char b);

int pim435_show(pim435_context *context);

#endif // PIM453_H_
